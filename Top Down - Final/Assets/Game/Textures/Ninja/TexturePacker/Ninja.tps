<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.11.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Sprite Sheet/Ninja.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../Images/Back - Hurt/Back - Hurt_000.png</key>
            <key type="filename">../Images/Back - Hurt/Back - Hurt_001.png</key>
            <key type="filename">../Images/Back - Hurt/Back - Hurt_002.png</key>
            <key type="filename">../Images/Back - Hurt/Back - Hurt_003.png</key>
            <key type="filename">../Images/Back - Hurt/Back - Hurt_004.png</key>
            <key type="filename">../Images/Back - Hurt/Back - Hurt_005.png</key>
            <key type="filename">../Images/Back - Hurt/Back - Hurt_006.png</key>
            <key type="filename">../Images/Back - Hurt/Back - Hurt_007.png</key>
            <key type="filename">../Images/Back - Hurt/Back - Hurt_008.png</key>
            <key type="filename">../Images/Back - Hurt/Back - Hurt_009.png</key>
            <key type="filename">../Images/Back - Hurt/Back - Hurt_010.png</key>
            <key type="filename">../Images/Back - Hurt/Back - Hurt_011.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_000.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_001.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_002.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_003.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_004.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_005.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_006.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_007.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_008.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_009.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_010.png</key>
            <key type="filename">../Images/Back - Idle/Back - Idle_011.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_000.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_001.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_002.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_003.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_004.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_005.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_006.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_007.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_008.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_009.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_010.png</key>
            <key type="filename">../Images/Back - Slashing/Back - Slashing_011.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_000.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_001.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_002.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_003.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_004.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_005.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_006.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_007.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_008.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_009.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_010.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_011.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_012.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_013.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_014.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_015.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_016.png</key>
            <key type="filename">../Images/Back - Walking/Back - Walking_017.png</key>
            <key type="filename">../Images/Dying/Dying_000.png</key>
            <key type="filename">../Images/Dying/Dying_001.png</key>
            <key type="filename">../Images/Dying/Dying_002.png</key>
            <key type="filename">../Images/Dying/Dying_003.png</key>
            <key type="filename">../Images/Dying/Dying_004.png</key>
            <key type="filename">../Images/Dying/Dying_005.png</key>
            <key type="filename">../Images/Dying/Dying_006.png</key>
            <key type="filename">../Images/Dying/Dying_007.png</key>
            <key type="filename">../Images/Dying/Dying_008.png</key>
            <key type="filename">../Images/Dying/Dying_009.png</key>
            <key type="filename">../Images/Dying/Dying_010.png</key>
            <key type="filename">../Images/Dying/Dying_011.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_000.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_001.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_002.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_003.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_004.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_005.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_006.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_007.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_008.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_009.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_010.png</key>
            <key type="filename">../Images/Front - Hurt/Front - Hurt_011.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_000.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_001.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_002.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_003.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_004.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_005.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_006.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_007.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_008.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_009.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_010.png</key>
            <key type="filename">../Images/Front - Idle Blinking/Front - Idle Blinking_011.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_000.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_001.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_002.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_003.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_004.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_005.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_006.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_007.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_008.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_009.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_010.png</key>
            <key type="filename">../Images/Front - Idle/Front - Idle_011.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_000.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_001.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_002.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_003.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_004.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_005.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_006.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_007.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_008.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_009.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_010.png</key>
            <key type="filename">../Images/Front - Slashing/Front - Slashing_011.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_000.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_001.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_002.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_003.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_004.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_005.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_006.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_007.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_008.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_009.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_010.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_011.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_012.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_013.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_014.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_015.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_016.png</key>
            <key type="filename">../Images/Front - Walking/Front - Walking_017.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_000.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_001.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_002.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_003.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_004.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_005.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_006.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_007.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_008.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_009.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_010.png</key>
            <key type="filename">../Images/Left - Hurt/Left - Hurt_011.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_000.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_001.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_002.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_003.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_004.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_005.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_006.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_007.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_008.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_009.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_010.png</key>
            <key type="filename">../Images/Left - Idle Blinking/Left - Idle Blinking_011.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_000.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_001.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_002.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_003.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_004.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_005.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_006.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_007.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_008.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_009.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_010.png</key>
            <key type="filename">../Images/Left - Idle/Left - Idle_011.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_000.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_001.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_002.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_003.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_004.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_005.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_006.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_007.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_008.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_009.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_010.png</key>
            <key type="filename">../Images/Left - Slashing/Left - Slashing_011.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_000.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_001.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_002.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_003.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_004.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_005.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_006.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_007.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_008.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_009.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_010.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_011.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_012.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_013.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_014.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_015.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_016.png</key>
            <key type="filename">../Images/Left - Walking/Left - Walking_017.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_000.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_001.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_002.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_003.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_004.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_005.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_006.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_007.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_008.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_009.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_010.png</key>
            <key type="filename">../Images/Right - Hurt/Right - Hurt_011.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_000.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_001.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_002.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_003.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_004.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_005.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_006.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_007.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_008.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_009.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_010.png</key>
            <key type="filename">../Images/Right - Idle Blinking/Right - Idle Blinking_011.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_000.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_001.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_002.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_003.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_004.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_005.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_006.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_007.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_008.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_009.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_010.png</key>
            <key type="filename">../Images/Right - Idle/Right - Idle_011.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_000.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_001.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_002.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_003.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_004.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_005.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_006.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_007.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_008.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_009.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_010.png</key>
            <key type="filename">../Images/Right - Slashing/Right - Slashing_011.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_000.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_001.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_002.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_003.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_004.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_005.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_006.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_007.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_008.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_009.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_010.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_011.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_012.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_013.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_014.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_015.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_016.png</key>
            <key type="filename">../Images/Right - Walking/Right - Walking_017.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../Images/Back - Hurt/Back - Hurt_000.png</filename>
            <filename>../Images/Back - Hurt/Back - Hurt_001.png</filename>
            <filename>../Images/Back - Hurt/Back - Hurt_002.png</filename>
            <filename>../Images/Back - Hurt/Back - Hurt_003.png</filename>
            <filename>../Images/Back - Hurt/Back - Hurt_004.png</filename>
            <filename>../Images/Back - Hurt/Back - Hurt_005.png</filename>
            <filename>../Images/Back - Hurt/Back - Hurt_006.png</filename>
            <filename>../Images/Back - Hurt/Back - Hurt_007.png</filename>
            <filename>../Images/Back - Hurt/Back - Hurt_008.png</filename>
            <filename>../Images/Back - Hurt/Back - Hurt_009.png</filename>
            <filename>../Images/Back - Hurt/Back - Hurt_010.png</filename>
            <filename>../Images/Back - Hurt/Back - Hurt_011.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_000.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_001.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_002.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_003.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_004.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_005.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_006.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_007.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_008.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_009.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_010.png</filename>
            <filename>../Images/Back - Idle/Back - Idle_011.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_000.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_001.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_002.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_003.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_004.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_005.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_006.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_007.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_008.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_009.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_010.png</filename>
            <filename>../Images/Back - Slashing/Back - Slashing_011.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_007.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_008.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_009.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_010.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_011.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_012.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_013.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_014.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_015.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_016.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_017.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_000.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_001.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_002.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_003.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_004.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_005.png</filename>
            <filename>../Images/Back - Walking/Back - Walking_006.png</filename>
            <filename>../Images/Dying/Dying_001.png</filename>
            <filename>../Images/Dying/Dying_002.png</filename>
            <filename>../Images/Dying/Dying_003.png</filename>
            <filename>../Images/Dying/Dying_004.png</filename>
            <filename>../Images/Dying/Dying_005.png</filename>
            <filename>../Images/Dying/Dying_006.png</filename>
            <filename>../Images/Dying/Dying_007.png</filename>
            <filename>../Images/Dying/Dying_008.png</filename>
            <filename>../Images/Dying/Dying_009.png</filename>
            <filename>../Images/Dying/Dying_010.png</filename>
            <filename>../Images/Dying/Dying_011.png</filename>
            <filename>../Images/Dying/Dying_000.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_002.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_003.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_004.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_005.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_006.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_007.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_008.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_009.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_010.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_011.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_000.png</filename>
            <filename>../Images/Front - Hurt/Front - Hurt_001.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_000.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_001.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_002.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_003.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_004.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_005.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_006.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_007.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_008.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_009.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_010.png</filename>
            <filename>../Images/Front - Idle/Front - Idle_011.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_004.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_005.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_006.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_007.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_008.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_009.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_010.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_011.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_000.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_001.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_002.png</filename>
            <filename>../Images/Front - Idle Blinking/Front - Idle Blinking_003.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_001.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_002.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_003.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_004.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_005.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_006.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_007.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_008.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_009.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_010.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_011.png</filename>
            <filename>../Images/Front - Slashing/Front - Slashing_000.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_008.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_009.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_010.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_011.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_012.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_013.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_014.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_015.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_016.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_017.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_000.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_001.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_002.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_003.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_004.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_005.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_006.png</filename>
            <filename>../Images/Front - Walking/Front - Walking_007.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_002.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_003.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_004.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_005.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_006.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_007.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_008.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_009.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_010.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_011.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_000.png</filename>
            <filename>../Images/Left - Hurt/Left - Hurt_001.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_004.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_005.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_006.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_007.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_008.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_009.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_010.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_011.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_000.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_001.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_002.png</filename>
            <filename>../Images/Left - Idle/Left - Idle_003.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_005.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_006.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_007.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_008.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_009.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_010.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_011.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_000.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_001.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_002.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_003.png</filename>
            <filename>../Images/Left - Idle Blinking/Left - Idle Blinking_004.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_002.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_003.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_004.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_005.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_006.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_007.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_008.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_009.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_010.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_011.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_000.png</filename>
            <filename>../Images/Left - Slashing/Left - Slashing_001.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_008.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_009.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_010.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_011.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_012.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_013.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_014.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_015.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_016.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_017.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_000.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_001.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_002.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_003.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_004.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_005.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_006.png</filename>
            <filename>../Images/Left - Walking/Left - Walking_007.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_002.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_003.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_004.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_005.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_006.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_007.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_008.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_009.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_010.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_011.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_000.png</filename>
            <filename>../Images/Right - Hurt/Right - Hurt_001.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_003.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_004.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_005.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_006.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_007.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_008.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_009.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_010.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_011.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_000.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_001.png</filename>
            <filename>../Images/Right - Idle/Right - Idle_002.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_003.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_004.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_005.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_006.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_007.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_008.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_009.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_010.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_011.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_000.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_001.png</filename>
            <filename>../Images/Right - Idle Blinking/Right - Idle Blinking_002.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_005.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_006.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_007.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_008.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_009.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_010.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_011.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_000.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_001.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_002.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_003.png</filename>
            <filename>../Images/Right - Slashing/Right - Slashing_004.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_007.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_008.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_009.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_010.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_011.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_012.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_013.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_014.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_015.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_016.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_017.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_000.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_001.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_002.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_003.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_004.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_005.png</filename>
            <filename>../Images/Right - Walking/Right - Walking_006.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
